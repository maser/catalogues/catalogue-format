# MASER Radio Catalogues

This repository describes the MASER Radio Catalogue model and its various implementations.

- [Assessment of existing catalogue formats](formats.md)
- [TFCat model (short version)](model.md)
- [TFCat model (full specification)](json/spec.md)

