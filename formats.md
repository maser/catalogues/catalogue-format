# Raw tabular text transcripts

Many catalogues has been published as text tables in journal articles in a tabular format. Examples od such
catalogues:
- Aubier, M. G. and Genova, F. (1985). A catalogue of the high frequency limit of the Jovian decameter
  emission observed by Voyager. Astron. Astrophys. Suppl. 61, 341–351. 
  ads:[1985A&AS...61..341A](https://articles.adsabs.harvard.edu/pdf/1985A%26AS...61..341A)
- Barrow, C. H. (1981). A catalogue of Jupiter’s decametric emission observed by Voyager-1 and by
  Voyager-2 in the range 15-40 MHz. Astron. Astrophys. Suppl. 46, 111–114. 
  ads:[1981A&AS...46..111B](https://articles.adsabs.harvard.edu/pdf/1981A%26AS...46..111B)
- Barrow, C. H. and Desch, M. D. (1980). Non-Io decametric radiation from Jupiter at frequencies above 30
  MHz. Astron. Astrophys. Suppl. 86, 339–341. 
  ads:[1980A&A....86..339B](https://articles.adsabs.harvard.edu/pdf/1980A%26A....86..339B)
- Bernold, T. (1980). A catalogue of fine structures in type IV solar radio bursts. Astron. Astrophys. Suppl.
  42, 43–58. ads:[1980A&AS...42...43B](https://articles.adsabs.harvard.edu/pdf/1980A%26AS...42...43B)
- Fischer, G., Pagaran, J. A., Zarka, P., Delcroix, M., Dyudina, U. A., Kurth, W. S. and  Gurnett, D. A.
  (2019). Analysis of a long-lived, two-cell lightning storm on Saturn. Astron. Astrophys. 621, A113. 
  doi:[10.1051/0004-6361/201833014](https://doi.org/10.1051/0004-6361/201833014) 
  vizier:[J/A+A/621/A113](https://doi.org/10.26093/cds/vizier.36210113)
- Kayser, S. E., Bougeret, J. L., Fainberg, J., and Stone, R. G. (1988). A catalog of interplanetary type III
  storms. Astron. Astrophys. Suppl. 73, 243–253. 
  ads:[1988A&AS...73..243K](https://articles.adsabs.harvard.edu/pdf/1988A%26AS...73..243K)
- Leblanc, Y., de La Noe, J., Genova, F., Gerbault, A., and Lecacheux, A. (1981). A catalogue of Jovian
  decametric radio observations from January 1978 to December 1979. Astron. Astrophys. Suppl. 46,
  135–149. ads:[1981A&AS...46..135L](https://articles.adsabs.harvard.edu/pdf/1981A%26AS...46..135L)
- Leblanc, Y., Gerbault, A., Denis, L., and Lecacheux, A. (1993). A catalogue of Jovian decametric radio
  observations from January 1988 to December 1990. Astron. Astrophys. Suppl. 98, 529–546. 
  ads:[1993A&AS...98..529L](https://articles.adsabs.harvard.edu/pdf/1993A%26AS...98..529L)
- Leblanc, Y., Gerbault, A., and Lecacheux, A. (1989). A catalogue of Jovian decametric radio observations
  from January 1982 to December 1984. Astron. Astrophys. Suppl. 77, 425–438. 
  ads:[1989A&AS...77..425L](https://articles.adsabs.harvard.edu/pdf/1989A%26AS...77..425L)
- Leblanc, Y., Gerbault, A., Lecacheux, A., and Boudjada, M. Y. (1990). A catalogue of Jovian decametric
  radio observations from January 1985 to December 1987. Astron. Astrophys. Suppl. 86, 191–207. 
  ads:[1990A&AS...86..191L](https://articles.adsabs.harvard.edu/pdf/1990A%26AS...86..191L)
- Leblanc, Y., Gerbault, A., Rubio, M., and Genova, F. (1983). A catalogue of Jovian radio observations
  from January 1980 to December 1981. Astron. Astrophys. Suppl. 54, 135–148. 
  ads:[1983A&AS...54..135L](https://articles.adsabs.harvard.edu/pdf/1983A%26AS...54..135L)
- Lobzin, V. V., Cairns, I. H., Robinson, P. A.,  Steward, G. and Patterson, G. (2010). Automatic 
  Recognition of Coronal Type II Radio Bursts: The Automated Radio Burst Identification System Method 
  and First ObservationsAstrophys. J. Lett., 710, Issue 1, L58-L62. 
  doi:[10.1088/2041-8205/710/1/L58](https://doi.org/10.1088/2041-8205/710/1/L58)
  vizier:[J/ApJ/710/L58](https://doi.org/10.26093/cds/vizier.17109058)
- Marques, M. S., Zarka, P., Echer, E., Ryabov, V. B., Alves, M. V., Denis, L., et al. (2017). Statistical
  analysis of 26 yr of observations of decametric radio emissions from Jupiter. Astron. Astrophys. 604,
  A17. doi:[10.1051/0004-6361/201630025](https://doi.org/10.1051/0004-6361/201630025)
  vizier:[J/A+A/604/A17](https://doi.org/10.26093/cds/vizier.36040017)
- Maxwell, A., Hughes, M. P., and Thompson, A. R. (1963). Catalog of Type II(Slow-Drift) and Type
  IV(Continuum) Solar Radio Bursts. J. Geophys. Res. 68, 1347–1354. 
  doi:[10.1029/JZ068i005p01347](https://doi.org/10.1029/JZ068i005p01347)

Some tables are already available through [Vizier](http://vizier.u-strasbg.fr), as noted in the reference 
list above. 

# VOTable
The [VOTable](http://www.ivoa.net/documents/VOTable/) format is a convenient format for sharing tabular data 
with virtual observatory (VO) tools, especially with [TOPCAT](http://www.star.bris.ac.uk/~mbt/topcat/). This 
format is well suited for simple feature geometries such as a temporal-spectral bounding box (a time interval 
and a spectral range). VOTable can host more complex spatial geometries (e.g., in sky coordinates), however 
the underlying data models don't allow extending this capability to complex temporal-spectral geometries. 

# HPEventList
The HPEventList format has been proposed by the [IHDEA](https://ihdea.net) as a standard for sharing heliophysics
related event lists. The HPEventList recommendation proposes 2 formats: a simple text format and a VOTable format. 
The restrictions of VOTable thus apply. 

# Spectro-temporal contours
While old catalogues are only describing the bounding box of the observed emissions in the time-frequency domain, 
more recent catalogues provide an accurate contour of each observed event. Before the definition of TFCat, the 
following solutions (or options) have been used (or explored):
* A *list of coordinate pairs* `(t,f)` defining a closed polygon in a time-frequency spectrogram representation. 
  This is the most generic form, but it is not standardized.
* Following the work done within the HELIO-HFC (Heliophyics Feature Catalogue), a *chaincode* representation can 
  be used. This is applicable on raster images of spectrograms. The coordinates of the pixel grid is required to 
  convert to a generic polygon.
* The IVOA is proposing polygon based on their STC (Space Time Coordinate) data model, currently for sky 
  coordinates or generic 2D spatial coordinates. Extension of such a representation is possible but it is not 
  clear if clients can use this format easily.
* The IVOA has also defined the STMOC (Space-Time Multi-Order-Coverage) standard, which could be extended with 
  an extra spectral dimension with a frequency scale (e.g., ET-MOC - Energy-Temporal MOC), allowing easy 
  representation of MASER Catalogue products. 
* The OGC (Open Geospatial Consortium) have developed geospatial data interchange format using JSON: GeoJSON 
  ([RFC 7946](https://tools.ietf.org/html/rfc7946)). This format can handle polygons in predefined coordinate 
  reference systems (CRS). This JSON format could easily be extended to spectro-temporal contours, although the
  resulting file would not be compliant with the GeoJSON specification.

# TFCat
The TFCat specification has been built on the GeoJSON model, adding interoperability with IVOA and IHDEA when 
applicable. 
