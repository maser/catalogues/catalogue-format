# Model Specification for Radio Time-Frequency Catalogues

## Introduction

A _radio time-frequency catalogue_ is a series of features in the time-frequency 
plane, as observed from a radio instrument (a ground based telescope or onboard a 
spacecraft), and identified by an agent (a scientist or a software). 

Each feature of the catalogue is defined by a set of temporal and spectral 
coordinates.

Features can have additional properties, depending on the science case, which content 
shall be defined in the catalogue. 

The temporal and spectral coordinate frame in use in the catalogue shall be defined uniquely 
in a catalogue file, or identified by unique coordinate system name. 

The catalogue can have general properties (e.g., header type metadata). 

## Classes of Features 

We can define several classes of features:
- a `Point` is defined by a single pair of coordinate (one time and one frequency);
- a `MultiPoint` is defined as a disconnected list of `Point` features;   
- a `LineString` is defined by a list of connected `Point` features, forming a trace;
- a `MultiLineString` is defined as a disconnected list of `LineString` features;
- a `Polygon` is defined by a closed list of connected `Point` features, forming a contour;
- a `MultiPolygon` is defined as a disconnected list of `Polygon` features;

NB: the feature class definition are inspired from the GeoJSON specification.
